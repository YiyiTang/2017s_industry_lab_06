@startuml

title Sphere - Class Diagram


class  Sphere{
  -double radius
  -double[] coordinates
  -{static} int numInstance
  
  +Sphere(double radius, double x, double y, double z)
  +{static} int count()
  +double area()
  +double volume()
}


@enduml